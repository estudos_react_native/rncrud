import {getActionFromState} from '@react-navigation/native';
import React, {useContext} from 'react';
import {FlatList, Text, View, StyleSheet, Alert} from 'react-native';
import {ListItem, Avatar, Button, Icon} from 'react-native-elements';
import UsersContext from '../context/UserContext';

export default (props) => {
  const {state, dispatch} = useContext(UsersContext);
  // console.warn(Object.keys(props));

  function confirmUserDeletion(user) {
    Alert.alert('Excluir Usuário', 'Deseja excluir o usuário ?', [
      {
        text: 'Sim',
        onPress() {
          dispatch({
            type: 'deleteUser',
            payload: user,
          });
        },
      },
      {
        text: 'Não',
      },
    ]);
  }

  function getUserItem({item}) {
    return (
      <ListItem
        bottomDivider
        onPress={() => props.navigation.navigate('UserForm', item)}>
        <Avatar source={{uri: item.avatarUrl}} />
        <ListItem.Content>
          <ListItem.Title>{item.name}</ListItem.Title>
          <ListItem.Subtitle>{item.email}</ListItem.Subtitle>
        </ListItem.Content>
        <Icon
          name="edit"
          size={25}
          color="orange"
          onPress={() => props.navigation.navigate('UserForm', item)}
        />
        <Icon
          name="delete"
          size={25}
          color="red"
          onPress={() => confirmUserDeletion(item)}
        />
        {/* <ListItem.Chevron /> */}
      </ListItem>
    );
  }

  return (
    <View>
      <FlatList
        keyExtractor={(user) => user.id.toString()}
        data={state.users}
        renderItem={getUserItem}
      />
    </View>
  );
};
