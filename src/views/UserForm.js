import React, {useContext, useState} from 'react';
import {Text, View, StyleSheet, Button} from 'react-native';
import {TextInput} from 'react-native-gesture-handler';
import UsersContext from '../context/UserContext';

export default ({route, navigation}) => {
  console.warn(Object.keys(route));
  const [user, setUser] = useState(route.params ? route.params : {});
  const {dispatch} = useContext(UsersContext);
  return (
    <View style={style.form}>
      <Text>Name</Text>
      <TextInput
        onChangeText={(name) => setUser({...user, name})}
        placeholder="Informe o nome"
        value={user.name}
        style={style.input}
      />
      <Text>Email</Text>
      <TextInput
        onChangeText={(email) => setUser({...user, email})}
        placeholder="Informe o email"
        value={user.email}
        style={style.input}
      />
      <Text>URL do Avatar</Text>
      <TextInput
        onChangeText={(avatarUrl) => setUser({...user, avatarUrl})}
        placeholder="Informe a URL do Avatar"
        value={user.avatarUrl}
        style={style.input}
      />
      <Button
        title="Salvar"
        onPress={() => {
          dispatch({
            type: user.id ? 'updateUser' : 'createUser',
            payload: user,
          });
          navigation.goBack();
        }}
      />
    </View>
  );
};

const style = StyleSheet.create({
  form: {
    padding: 12,
  },
  input: {
    height: 40,
    borderColor: 'gray',
    borderWidth: 1,
    marginBottom: 15,
  },
});
