export default [
  {
    id: 1,
    name: 'João Silva',
    email: 'josil@email.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2016/03/31/19/58/avatar-1295429_960_720.png',
  },
  {
    id: 2,
    name: 'Morena Soares',
    email: 'morena@email.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2020/12/07/13/34/woman-5811616_960_720.png',
  },
  {
    id: 3,
    name: 'Amanada Peixoto',
    email: 'amanda@email.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2020/12/11/20/55/woman-5823815_960_720.png',
  },
  {
    id: 4,
    name: 'Ruiva Chata',
    email: 'ruiva@email.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2016/08/28/13/12/secondlife-1625903_960_720.jpg',
  },
  {
    id: 5,
    name: 'Carla Silva',
    email: 'carla@email.com',
    avatarUrl:
      'https://cdn.pixabay.com/photo/2020/12/08/21/48/woman-5815683_960_720.jpg',
  },
];
